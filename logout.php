<?php
session_start();
$path = pathinfo( $_SERVER['PHP_SELF'] );
$dir_name = $path['dirname'].'/';
$php_file = $path['basename'];
session_destroy();
header("Location: {$dir_name}login.php");		
?>