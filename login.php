	<?php
    if(isset($_GET['auth'])){
		require_once('handler/functions.php');
		echo json_encode(authenticate($_POST['username'],$_POST['password']));
		exit();
	}
	?>
    <?php include('header.php');?>
        
              <div class="row-fluid">
                <div class="span4 offset4">
                  <div class="signin">
                    <form class="signin-wrapper" id="authenticate-frm">
                      
                      <div class="content">
                        Username: <input class="input input-block-level"  name="username" id="username" type="text" value="">
                        Password: <input class="input input-block-level"  name="password" id="password" type="password">
                      </div>
                      <div id="authenticate-status"></div>
                      <div class="actions">
                        <a id="authenticate-btn" class="btn btn-info pull-right" >Login</a>
                        <div class="clearfix"></div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            
   		    <?php include('footer.php');?> 
   
            <script type="text/javascript" >
			$(document).keypress(function(event){
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if(keycode == '13'){
					authenticate();
				}
			});		
			$(document).ready(function(){
				$("#authenticate-btn").click(function(){
					authenticate();
				});
			});
			function authenticate(){
				$("#authenticate-status").html('<div class="ajax-loader-small"></div>');
				$.ajax({
					type : "POST",
					url: "login.php?auth=1",
					data: $("#authenticate-frm").serialize(),
					success: function(data){
						var new_data = $.parseJSON(data);
						if(new_data.status){
							window.location = 'index.php';
						}else{
							$("#authenticate-status").html('<div class="alert alert-block alert-error fade in"><span class="fs1" aria-hidden="true" data-icon=""></span> '+new_data.msg+'</div>');	
						}
					}	
				});
			}
            </script>
         
         
