/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
	// Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
		url: 'handler/',
		success: function (data) {
			$('#error-modal-body').removeClass('alert alert-success alert-danger');
			if(!data.status){
				$('#error-modal').modal('show');
				$('#error-modal-body').addClass('alert alert-danger');
				$('#error-modal-title').html('Error');
				$('#error-modal-body').html('<strong>'+data.files[0].name +'</strong> '+ data.msg);	
			}else{
				$('#error-modal-body').addClass('alert alert-success');
				$('#error-modal-title').html('Result');
				$('#error-modal-body').html('<div><strong>'+data.files[0].name +'</strong></div><div>Saved: '+data.saved +'</div><div>Duplicates: '+data.duplicate +'</div>');
				
				setTimeout(function(){
					$('#error-modal').modal('show');
					if(data.saved > 0){
						setTimeout(function(){get_records();},1000);
					}
				},1000);
			}
		},
		error: function (request, status, error) {
			if(request.status == 200){
			$('#error-modal').modal('show');
			$('#error-modal-body').addClass('alert alert-danger');
			$('#error-modal-title').html('Error');
			$('#error-modal-body').html("Server timeout, please select CSV with minimal record to prevent error in the future.");	
			}
		}
    });
});
