            <?php  if(!$data):?>
                <div class="alert alert-block alert-error" align="center">No records found.</div>
			<?php else:  ?>
            <div id="dt_example" class="example_alt_pagination">
                <table class="table table-condensed table-striped table-hover table-bordered" id="data-table">    
                    <thead>
                        <tr>
                            <th >Email</th>
                            <?php /*?><th >Name</th><?php */?>
                            <th >Message Number</th>
                            <th >Date Added</th>
                            <th >Last Followup</th>
                            <?php /*?><th >Stop Time</th><?php */?>
                            <th >Stop Status</th>
                            <?php /*?><th >Misc</th><?php */?>
                            <th >Ad</th>
                            <th >IP</th>
                            <?php /*?><th >URL</th><?php */?>
                            <th >Country</th>
                            <th >Region</th>
                            <th >City</th>
                            <?php /*?><th >PostalCode</th><?php */?>
                            <th >Latitude</th>
                            <th >Longitude</th>
                            <th >DMA Code</th>
                            <th >AreaCode</th>
                        </tr>
                    </thead>
                    <tbody id="">
                        
						<?php foreach($data as $k => $val):?>
                        <tr>
                            <td><?php echo $val['Email'];?></td>
                            <?php /*?><td><?php echo $val['Name'];?></td><?php */?>
                            <td><?php echo $val['Message_Number'];?></td>
                            <td><?php echo $val['Date_Added'];?></td>
                            <td><?php echo $val['Last_Followup_Date'];?></td>
                            <?php /*?><td><?php echo $val['Stop_Time'];?></td><?php */?>
                            <td><?php echo $val['Stop_Status'];?></td>
                            <?php /*?><td><?php echo $val['Misc'];?></td><?php */?>
                            <td><?php echo $val['Ad_Tracking'];?></td>
                            <td><?php echo $val['IP_Address'];?></td>
                            <?php /*?><td><?php echo $val['Web_Form_URL'];?></td><?php */?>
                            <td><?php echo $val['Country'];?></td>
                            <td><?php echo $val['Region'];?></td>
                            <td><?php echo $val['City'];?></td>
                            <?php /*?><td><?php echo $val['Postal_Code'];?></td><?php */?>
                            <td><?php echo $val['Latitude'];?></td>
                            <td><?php echo $val['Longitude'];?></td>
                            <td><?php echo $val['DMA_Code'];?></td>
                            <td><?php echo $val['Area_Code'];?></td>
                        </tr>
                        <?php endforeach;?>
                        
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            
            
            
			
			<script>
			$(document).ready(function () {
								
				$('#data-table').dataTable({
					//"bDestroy": true,
					//"bRetrieve": true,
					//"bSort": false,
					//"aoColumnDefs": [{ bSortable: false, aTargets: [ 0,-1 ] } ],
					//"bInfo": false,
					"bProcessing": true,
					"bFilter": true,
					"iDisplayLength": 10,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ 3, "desc" ]]
				});
				
			});
			
			</script>
            <?php endif;?>
            
 
