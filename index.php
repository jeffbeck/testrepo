	<?php
    if(isset($_GET['getdata'])){
        require('handler/functions.php');
		$data = get_records();
		require_once('records-view.php');
		exit();
    }
    ?>
    
    
    <?php include('header.php');?>
    	
        <div class="dashboard-wrapper">
        
        	<?php include('header-view.php');?>
        
            <div class="main-container">
                
                <div class="row-fluid">
                    <div class="span12" >
                        
                        <!-- The file upload form used as target for the file upload widget -->
                        <form id="fileupload" method="POST" enctype="multipart/form-data" >
                            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                             
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-small btn-success fileinput-button pull-left">
                                <span class="fs1" data-icon="" aria-hidden="true"></span>
                                <span>Select CSV File</span>
                                <input type="file" name="files[]" multiple>
                            </span>
                            
                            <div class="clearfix"></div>
                            
                            <!-- The table listing the files available for upload/download -->
                            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                        </form>
                        
                    </div>
                </div>
                
                <div class="row-fluid">    
                    <div class="span12" id="records">

                        <div class="widget">
                            <div class="widget-header">
                                <div class="title">Records</div>
                            </div>
                            <div class="widget-body" id="records-body"></div>
                        </div>
                    
                    </div>
                </div>  
                    
            </div><!--main-container-->
        
        </div><!-- dashboard-wrapper -->
    
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade" >
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">{%=file.name%}</p>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-success progress-striped active"><div class="bar" role="progressbar" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-small btn-primary start" disabled>
                        <span class="fs1" data-icon="" aria-hidden="true"></span>
                        <span>Upload</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-small btn-warning cancel">
                        <span class="fs1" data-icon="" aria-hidden="true"></span>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade" >
            <td>
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                    <button class="btn btn-small btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <span class="fs1" data-icon="" aria-hidden="true"></span>
                        <span>Remove</span>
                    </button>
                {% } else { %}
                    <button class="btn btn-small btn-warning cancel">
                        <span class="fs1" data-icon="" aria-hidden="true"></span>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
        {% if (file.error) { %}
            <div><span class="label label-danger">Error</span> {%=file.error%}</div>
        {% } %}
    
    {% } %}
    </script>
    
   <?php include('footer.php');?> 
   
    <script>
    $(document).ready(function(){
		get_records();
	});
	function get_records(){
		$('#records-body').html('<img src="img/ajax-loader.gif" style="margin:50px 50% 50px 45%;" />');
		$.ajax({
			type: "GET",
			url: "index.php?getdata=1",
			success: function(data){
				$('#records-body').html(data);
			},
			error: function(data){
				$('#records-body').html('<span class="alert alert-block alert-error">An error occured please try again.</span>');
			}	
		});
	}
    </script>
	<script src="js/jquery.dataTables.js"></script>
   
  
         
         
